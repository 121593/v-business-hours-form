"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "VBusinessHoursForm", {
  enumerable: true,
  get: function get() {
    return _VBusinessHoursForm["default"];
  }
});
Object.defineProperty(exports, "VBusinessHoursFormRow", {
  enumerable: true,
  get: function get() {
    return _VBusinessHoursFormRow["default"];
  }
});
exports["default"] = exports.VBHF_EVENTS = void 0;

var _VBusinessHoursForm = _interopRequireDefault(require("./VBusinessHoursForm"));

var _VBusinessHoursFormRow = _interopRequireDefault(require("./VBusinessHoursFormRow"));

var _VTimePickerGroup = _interopRequireDefault(require("./VTimePickerGroup"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var VBusinessHoursFormElements = {
  install: function install(Vue, options) {
    Vue.component('v-business-hours-form', _VBusinessHoursForm["default"]);
    Vue.component('v-business-hours-form-row', _VBusinessHoursFormRow["default"]);
    Vue.component('v-time-picker-group', _VTimePickerGroup["default"]);
  }
};
var VBHF_EVENTS = {
  CREATE: 'updatevalue',
  DELETE: 'deletevalues',
  UPDATE: 'createvalueon'
};
exports.VBHF_EVENTS = VBHF_EVENTS;
var _default = VBusinessHoursFormElements;
exports["default"] = _default;

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VBusinessHoursFormElements);
}
//# sourceMappingURL=index.js.map