"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _localable = _interopRequireDefault(require("vuetify/lib/mixins/localable"));

var _themeable = _interopRequireDefault(require("vuetify/lib/mixins/themeable"));

var _lib = require("vuetify/lib");

var _index = require("./index");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var base = _vue["default"].extend({
  mixins: [_localable["default"], _themeable["default"]]
});

var _default2 = base.extend().extend({
  // export default mixins(
  //   Localable,
  //   Themeable,
  // ).extend({
  name: 'v-business-hours-form',
  props: {
    // Data
    initialBusinessHours: {
      type: Array,
      required: false
    },
    // Options :
    customStyle: {
      type: Object,
      "default": function _default() {
        return {};
      }
    },
    weekStartOn: {
      type: Number,
      "default": 0,
      validator: function validator(val) {
        return val === 0 || val === 6;
      }
    },
    showOnEmptyData: {
      type: Boolean,
      "default": true
    },
    formName: {
      type: String,
      "default": 'v-business-hours-form'
    }
  },
  data: function data() {
    return {
      businessHours: [],
      timePickerOpen: false,
      timePickerTarget: null
    };
  },
  computed: {
    idMappedBusinessHours: function idMappedBusinessHours() {
      return new Map(this.businessHours.map(function (v) {
        return [v.id, v];
      }));
    },
    dayIndexMappedBusinessHours: function dayIndexMappedBusinessHours() {
      var hours = new Map();
      this.businessHours.forEach(function (bh) {
        var di = bh.dayIndex;
        var businessHoursForToday = hours.has(di) ? hours.get(di) : [];
        businessHoursForToday.push(bh);
        hours.set(di, businessHoursForToday);
      });
      return hours;
    }
  },
  mounted: function mounted() {
    this.businessHours = this.initialBusinessHours;
  },
  methods: {
    /*
     Event handlers
      */
    handleValueChanged: function handleValueChanged(id, value) {
      var key = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      if (!this.idMappedBusinessHours.has(id)) {
        throw new Error("Hour id ".concat(id, " not found"));
      }

      var target = this.idMappedBusinessHours.get(id);

      if (key && typeof value === 'string') {
        // Changing only something
        target[key] = value;
      } else if (typeof value !== 'string') {
        target = value;
      } else {
        throw new Error('No key specified and string value supplied');
      }

      this.idMappedBusinessHours.set(id, target);
      this.$emit('change', this.businessHours);
    },
    removeValues: function removeValues(ids) {
      this.businessHours = this.businessHours.filter(function (_ref) {
        var id = _ref.id;
        return !ids.includes(id);
      });
      this.$emit('change', this.businessHours);
    },
    handleAddButtonClicked: function handleAddButtonClicked(dayIndex) {
      console.log('handleAddButtonClicked', dayIndex);
      this.businessHours.push({
        closeAt: '',
        openAt: '',
        dayIndex: dayIndex,
        // Simple random id
        id: "vbh-".concat(Math.random().toString(36).substring(7))
      });
      this.$emit('change', this.businessHours);
    },
    // Components generation
    genRows: function genRows() {
      if (!(this.businessHours && this.businessHours.length) && !this.showOnEmptyData) {
        console.warn('No business hours supplied and showOnEmptyData disabled');
        return null;
      }

      var rows = [];
      var WEEK_LENGTH = 7;

      for (var inputIndex = 0, i = 0; i < WEEK_LENGTH; i++) {
        var hours = this.dayIndexMappedBusinessHours.get(i.toString());
        rows.push(this.genRow(i, hours, inputIndex), this.$createElement(_lib.VDivider, {}));
        inputIndex += hours ? hours.length : 0;
      } // Reorder rows if week starts on Sunday


      if (this.weekStartOn === 6) {
        console.log('reorder');
        rows.unshift(rows.pop()); // Divider

        rows.unshift(rows.pop()); // Actual row
      }

      return rows;
    },
    genRow: function genRow(i) {
      var _on;

      var hours = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      var inputIndex = arguments.length > 2 ? arguments[2] : undefined;
      // noinspection SpellCheckingInspection
      return this.$createElement(_index.VBusinessHoursFormRow, {
        on: (_on = {}, _defineProperty(_on, _index.VBHF_EVENTS.UPDATE, this.handleValueChanged), _defineProperty(_on, _index.VBHF_EVENTS.DELETE, this.removeValues), _defineProperty(_on, _index.VBHF_EVENTS.CREATE, this.handleAddButtonClicked), _on),
        props: {
          hours: hours,
          dayIndex: i.toString(),
          inputIndexStart: inputIndex,
          formName: this.formName
        }
      });
    }
  },
  render: function render(h) {
    return h('div', {}, [this.genRows()]);
  }
});

exports["default"] = _default2;
//# sourceMappingURL=VBusinessHoursForm.js.map