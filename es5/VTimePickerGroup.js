"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _lib = require("vuetify/lib");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// Imported Types
/// <reference path="../node_modules/vuetify/src/globals.d.ts" />
/// <reference path="../node_modules/vuetify/types/index.d.ts" />
var _default = _vue["default"].extend({
  name: 'v-time-picker-group',
  props: {
    value: {
      type: String
    }
  },
  data: function data() {
    return {
      isActive: false
    };
  },
  methods: {
    /**
     * Generates value holder & menu activator
     * Called by ScopedSlot
     */
    genScopedTextField: function genScopedTextField() {
      var _this = this;

      return this.$createElement(_lib.VTextField, {
        props: {
          readonly: true,
          value: this.value,
          placeholder: 'xx:xx'
        },
        // @todo: take care of sizing via CSS ?
        attrs: {
          size: 5
        },
        on: {
          click: function click() {
            return _this.isActive = true;
          }
        }
      });
    },

    /**
     * Generates timePicker
     * Called by ScopedSlot
     */
    genScopedPicker: function genScopedPicker() {
      var _this2 = this;

      return this.$createElement(_lib.VTimePicker, {
        props: {
          format: '24hr',
          value: this.value
        },
        on: {
          change: function change(newVal) {
            _this2.$emit('change', newVal);

            _this2.isActive = false;
          }
        }
      });
    },

    /**
     * Generates VMenu holding timePicker and activator
     */
    genMenu: function genMenu() {
      var _this3 = this;

      return this.$createElement(_lib.VMenu, {
        props: {
          closeOnContentClick: false,
          value: this.isActive
        },
        scopedSlots: {
          activator: this.genScopedTextField,
          "default": this.$scopedSlots.picker ? this.$scopedSlots.picker : this.genScopedPicker
        },
        on: {
          // Prevents desync between this.isActive and menu internal state
          'update:return-value': function updateReturnValue() {
            return _this3.isActive = false;
          }
        }
      });
    }
  },
  render: function render(h) {
    return h(_lib.VLayout, {
      props: {
        row: true,
        wrap: true
      }
    }, [this.genMenu()]);
  }
});

exports["default"] = _default;
//# sourceMappingURL=VTimePickerGroup.js.map