"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _localable = _interopRequireDefault(require("vuetify/lib/mixins/localable"));

var _themeable = _interopRequireDefault(require("vuetify/lib/mixins/themeable"));

var _lib = require("vuetify/lib");

var _VTimePickerGroup = _interopRequireDefault(require("./VTimePickerGroup"));

require("../src/VBusinessHoursForm.scss");

var _CssClassAware = _interopRequireDefault(require("./mixins/CssClassAware"));

var _index = require("./index");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var base = _vue["default"].extend({
  mixins: [_localable["default"], _themeable["default"], _CssClassAware["default"]]
});

var _default = base.extend().extend({
  // export default mixins(
  //   Localable,
  //   Themeable,
  //   CssClassAware
  // ).extend({
  name: 'v-business-hours-form-row',
  props: {
    // Data
    hours: {
      type: Array,
      required: true
    },
    dayIndex: {
      type: String,
      required: true
    },
    // This props tracks the number of inputs previously output as HTML inputs array use indexes
    inputIndexStart: {
      type: Number,
      required: true
    },
    formName: {
      type: String,
      required: true
    },
    // Options
    plusIcon: {
      type: String,
      "default": '$plus'
    }
  },
  methods: {
    /**
     * Generates hidden inputs holdings values, for traditional form sending
     */
    genHiddenInputs: function genHiddenInputs() {
      var _this = this;

      var inputs = [];
      this.hours.forEach(function (h, i) {
        return [['dayOfWeek', _this.dayIndex], ['openAt', h.openAt], ['closeAt', h.closeAt]].forEach(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
              key = _ref2[0],
              value = _ref2[1];

          return inputs.push(_this.$createElement('input', {
            attrs: {
              type: 'hidden',
              name: "[".concat(_this.formName, "][").concat(_this.inputIndexStart + i, "][").concat(key, "]"),
              value: value
            }
          }));
        });
      });
      return inputs;
    },

    /**
     * @todo: Verify translation usage & naming
     * @param dayIndex
     */
    genDayContainer: function genDayContainer(dayIndex) {
      return this.$createElement('div', {
        "class": this.c('day')
      }, this.$vuetify.lang.t("$vuetify.vbusinesshours.day.".concat(dayIndex)));
    },
    genOpenCloseToggle: function genOpenCloseToggle(isOpen) {
      return this.$createElement(_lib.VSwitch, {
        on: {
          change: this.handleIsOpenChange
        },
        props: {
          inputValue: isOpen
        },
        "class": this.c('switch')
      });
    },
    genHours: function genHours(hours) {
      var _this2 = this;

      var hourNodes = hours.map(function (bh, i) {
        return _this2.$createElement('div', {
          "class": _this2.c('hour-group'),
          key: bh.id
        }, [_this2.genHourDisplay(bh.openAt, {
          id: bh.id,
          prop: 'openAt'
        }), _this2.$createElement('span', {
          "class": _this2.c('separator')
        }, '-'), _this2.genHourDisplay(bh.closeAt, {
          id: bh.id,
          prop: 'closeAt'
        }), // Add button is displayed after last hours group
        i === hours.length - 1 ? _this2.genHoursAddButton() : null]);
      });
      return this.$createElement('div', {
        "class": this.c('hour-container')
      }, hourNodes);
    },
    genHourDisplay: function genHourDisplay(hour, callbackParams) {
      return this.$createElement(_VTimePickerGroup["default"], {
        props: {
          value: hour
        },
        on: {
          change: this.handleHourChange(callbackParams.id, callbackParams.prop)
        }
      });
    },
    genHoursAddButton: function genHoursAddButton() {
      return this.$createElement(_lib.VBtn, {
        props: {
          icon: true,
          color: 'primary'
        },
        on: {
          click: this.handleAddHoursClick
        },
        "class": this.c('add-hours-button')
      }, [this.$createElement(_lib.VIcon, this.plusIcon)]);
    },
    // Event handlers

    /**
     * Handle IsOpen switch change
     * Triggers value creation or deletion
     * @param v Boolean Value
     */
    handleIsOpenChange: function handleIsOpenChange(v) {
      if (v) {
        // IsOpen
        this.handleAddHoursClick();
      } else {
        // !IsOpen
        this.$emit(_index.VBHF_EVENTS.DELETE, this.hours.map(function (h) {
          return h.id;
        }));
      }
    },

    /**
     * @fixme: this is ugly
     * Event handler generator
     * @param id String Value id
     * @param prop String Target prop name
     */
    handleHourChange: function handleHourChange(id, prop) {
      var _this3 = this;

      // console.log('handleHourChange', id, prop)
      return function (newVal) {
        var newState = _this3.hours;
        var elem = newState.find(function (v) {
          return v.id === id;
        });

        if (!elem) {
          throw new Error("Unable to find element id ".concat(id));
        }

        elem[prop] = newVal;

        _this3.$emit(_index.VBHF_EVENTS.UPDATE, id, newVal);
      };
    },
    handleAddHoursClick: function handleAddHoursClick() {
      this.$emit(_index.VBHF_EVENTS.CREATE, this.dayIndex);
    }
  },
  render: function render(h) {
    var children = [this.genHiddenInputs(), this.genDayContainer(this.dayIndex), this.genOpenCloseToggle(!!this.hours.length), this.genHours(this.hours)];
    return h('div', {
      "class": this.c('row')
    }, children);
  }
});

exports["default"] = _default;
//# sourceMappingURL=VBusinessHoursFormRow.js.map