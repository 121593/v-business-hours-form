export type VBusinessHourInterface = {
  id: string
  dayIndex: string
  openAt: string
  closeAt: string
}

export interface TimePickerTarget {
  id: string
  field: 'openAt' | 'closeAt'
}
