import Vue from 'vue'
import { Framework } from 'vuetify/types'

declare module 'vue/types/vue' {
  export interface Vue {
    $vuetify: Framework
  }
}
declare module 'vuetify/lib' {
  export interface Vue {
    $vuetify: Framework
  }
}
