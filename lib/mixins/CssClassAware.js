import Vue from 'vue';
export default Vue.extend({
  name: 'cssclassaware',
  methods: {
    /**
     * Class name generator
     * @todo: extract from class ? To a mixin ?
     * @param v
     */
    c: v => `v-bhf__${v}`
  }
});
//# sourceMappingURL=CssClassAware.js.map