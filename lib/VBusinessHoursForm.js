// Imported Types
/// <reference path="../node_modules/vuetify/src/globals.d.ts" />
/// <reference path="../node_modules/vuetify/types/index.d.ts" />
/// <reference path="../types/index.d.ts" />
import Vue from 'vue'; // @ts-ignore

import Localable from 'vuetify/lib/mixins/localable'; // @ts-ignore

import Themeable from 'vuetify/lib/mixins/themeable';
import { VDivider } from 'vuetify/lib';
import { VBusinessHoursFormRow, VBHF_EVENTS } from './index';
const base = Vue.extend({
  mixins: [Localable, Themeable]
});
export default base.extend().extend({
  // export default mixins(
  //   Localable,
  //   Themeable,
  // ).extend({
  name: 'v-business-hours-form',
  props: {
    // Data
    initialBusinessHours: {
      type: Array,
      required: false
    },
    // Options :
    customStyle: {
      type: Object,
      default: () => ({})
    },
    weekStartOn: {
      type: Number,
      default: 0,
      validator: val => val === 0 || val === 6
    },
    showOnEmptyData: {
      type: Boolean,
      default: true
    },
    formName: {
      type: String,
      default: 'v-business-hours-form'
    }
  },
  data: () => ({
    businessHours: [],
    timePickerOpen: false,
    timePickerTarget: null
  }),
  computed: {
    idMappedBusinessHours() {
      return new Map(this.businessHours.map(v => [v.id, v]));
    },

    dayIndexMappedBusinessHours() {
      const hours = new Map();
      this.businessHours.forEach(bh => {
        const di = bh.dayIndex;
        const businessHoursForToday = hours.has(di) ? hours.get(di) : [];
        businessHoursForToday.push(bh);
        hours.set(di, businessHoursForToday);
      });
      return hours;
    }

  },

  mounted() {
    this.businessHours = this.initialBusinessHours;
  },

  methods: {
    /*
     Event handlers
      */
    handleValueChanged(id, value, key = null) {
      if (!this.idMappedBusinessHours.has(id)) {
        throw new Error(`Hour id ${id} not found`);
      }

      let target = this.idMappedBusinessHours.get(id);

      if (key && typeof value === 'string') {
        // Changing only something
        target[key] = value;
      } else if (typeof value !== 'string') {
        target = value;
      } else {
        throw new Error('No key specified and string value supplied');
      }

      this.idMappedBusinessHours.set(id, target);
      this.$emit('change', this.businessHours);
    },

    removeValues(ids) {
      this.businessHours = this.businessHours.filter(({
        id
      }) => !ids.includes(id));
      this.$emit('change', this.businessHours);
    },

    handleAddButtonClicked(dayIndex) {
      console.log('handleAddButtonClicked', dayIndex);
      this.businessHours.push({
        closeAt: '',
        openAt: '',
        dayIndex,
        // Simple random id
        id: `vbh-${Math.random().toString(36).substring(7)}`
      });
      this.$emit('change', this.businessHours);
    },

    // Components generation
    genRows() {
      if (!(this.businessHours && this.businessHours.length) && !this.showOnEmptyData) {
        console.warn('No business hours supplied and showOnEmptyData disabled');
        return null;
      }

      const rows = [];
      const WEEK_LENGTH = 7;

      for (let inputIndex = 0, i = 0; i < WEEK_LENGTH; i++) {
        const hours = this.dayIndexMappedBusinessHours.get(i.toString());
        rows.push(this.genRow(i, hours, inputIndex), this.$createElement(VDivider, {}));
        inputIndex += hours ? hours.length : 0;
      } // Reorder rows if week starts on Sunday


      if (this.weekStartOn === 6) {
        console.log('reorder');
        rows.unshift(rows.pop()); // Divider

        rows.unshift(rows.pop()); // Actual row
      }

      return rows;
    },

    genRow(i, hours = [], inputIndex) {
      // noinspection SpellCheckingInspection
      return this.$createElement(VBusinessHoursFormRow, {
        on: {
          [VBHF_EVENTS.UPDATE]: this.handleValueChanged,
          [VBHF_EVENTS.DELETE]: this.removeValues,
          [VBHF_EVENTS.CREATE]: this.handleAddButtonClicked
        },
        props: {
          hours,
          dayIndex: i.toString(),
          inputIndexStart: inputIndex,
          formName: this.formName
        }
      });
    }

  },

  render(h) {
    return h('div', {}, [this.genRows()]);
  }

});
//# sourceMappingURL=VBusinessHoursForm.js.map