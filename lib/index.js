import VBusinessHoursForm from './VBusinessHoursForm';
import VBusinessHoursFormRow from './VBusinessHoursFormRow';
import VTimePickerGroup from './VTimePickerGroup';
const VBusinessHoursFormElements = {
  install(Vue, options) {
    Vue.component('v-business-hours-form', VBusinessHoursForm);
    Vue.component('v-business-hours-form-row', VBusinessHoursFormRow);
    Vue.component('v-time-picker-group', VTimePickerGroup);
  }

};
export const VBHF_EVENTS = {
  CREATE: 'updatevalue',
  DELETE: 'deletevalues',
  UPDATE: 'createvalueon'
};
export { VBusinessHoursForm, VBusinessHoursFormRow };
export default VBusinessHoursFormElements;

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VBusinessHoursFormElements);
}
//# sourceMappingURL=index.js.map