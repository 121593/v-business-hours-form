import Vue from 'vue'
import App from './App'
import Vuetify from 'vuetify'
import LoadScript from 'vue-plugin-load-script'
import VBusinessHoursForm from '../src'
import en from '../src/locale/en'

Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(LoadScript)
Vue.use(VBusinessHoursForm)

const vuetify = new Vuetify({
  theme: {
    dark: false,
  },
  icons: {
    iconfont: 'mdiSvg',
  },
  lang: {
    locales: { en },
  },
})

const vm = new Vue({
  data: () => ({ isLoaded: document.readyState === 'complete' }),
  vuetify,
  render (h) {
    return this.isLoaded ? h(App) : undefined
  },
}).$mount('#app')

// Prevent layout jump while waiting for styles
vm.isLoaded || window.addEventListener('load', () => {
  vm.isLoaded = true
})
