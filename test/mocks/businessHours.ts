const businessHours = [
  { id: 'a', dayIndex: '1', openAt: '08:00', closeAt: '12:00' },
  { id: 'b', dayIndex: '1', openAt: '14:00', closeAt: '18:00' },
]

export default businessHours
