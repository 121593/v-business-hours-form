import Vue, { VNode } from 'vue'
// Utils
import { MountOptions, shallowMount, Wrapper } from '@vue/test-utils'
// To be tested
import VBusinessHoursFormModule from '..'
import VBusinessHoursForm from '../VBusinessHoursForm'
// Mocks
import businessHours from '../../test/mocks/businessHours'
import { VBusinessHourInterface } from '../../types'

const vuetifyMocks = {
  $vuetify: {
    theme: {
      currentTheme: {
        error: '#ff0000',
      },
      dark: false,
    },
    lang: {
      t: (val: string) => val,
    },
    rtl: false,
  },

}

describe('VBusinessHoursForm', () => {
  describe('installer', () => {
    it('should register the v-business-hours-row component', () => {
      Vue.use(VBusinessHoursFormModule)
      expect(Vue.options.components['v-business-hours-form']).toBeTruthy()
    })
  })

  describe('component', () => {
    type Instance = InstanceType<typeof VBusinessHoursForm>
    let mountFunction: (options?: MountOptions<Instance>) => Wrapper<Instance>

    beforeEach(() => {
      mountFunction = (options?: MountOptions<Instance>) => {
        return shallowMount(VBusinessHoursForm, {
          mocks: {
            ...vuetifyMocks,
          },
          ...options,
        })
      }
    })

    describe('Typical use', () => {
      let wrapper: Wrapper<Instance>

      beforeEach(() => {
        wrapper = mountFunction({
          propsData: {
            initialBusinessHours: businessHours,
          },
        })
      })

      it('Update single values as expected', () => {
        const newOpeningHour = '10:00'
        const key = 'openAt'
        const valueId = 'a'
        wrapper.vm.handleValueChanged(valueId, newOpeningHour, key)

        const lookedUpValue = wrapper.vm.idMappedBusinessHours.get(valueId)
        expect(lookedUpValue).toBeDefined()
        expect((<VBusinessHourInterface>lookedUpValue)[key]).toBe(newOpeningHour)
      })

      it('Update full values as expected', () => {
        const valueId = 'a'
        const newValue: VBusinessHourInterface = { id: valueId, dayIndex: '1', openAt: '00:00', closeAt: '24:00' }
        wrapper.vm.handleValueChanged('a', newValue)
        expect(wrapper.vm.idMappedBusinessHours.get(valueId)).toBe(newValue)
      })

      it('Remove values as expected', () => {
        const values = ['a', 'b']
        wrapper.vm.removeValues(values)
        expect(wrapper.vm.businessHours).toHaveLength(0)
      })

      it('Create values as expected', () => {
        wrapper.vm.handleAddButtonClicked('0')
        expect(wrapper.vm.businessHours.filter(bh => bh.dayIndex === '0').length).toBeTruthy()
      })
    })

    describe('Weeks starts on sunday use', () => {
      let wrapper: Wrapper<Instance>

      beforeEach(() => {
        wrapper = mountFunction({
          propsData: {
            initialBusinessHours: businessHours,
            weekStartOn: 6,
          },
        })
      })

      it('Move sunday\'s row to the top', () => {
        const rows = <VNode[]>wrapper.vm.genRows()
        const firstRow = <VNode>rows.shift()

        const firstRowProps = <{dayIndex: string}>firstRow.data.props
        expect(firstRowProps.dayIndex).toBe('6')
      })
    })

    describe('No data supplied and !showOnEmptyData', () => {
      let wrapper: Wrapper<Instance>

      beforeEach(() => {
        wrapper = mountFunction({
          propsData: {
            initialBusinessHours: null,
            showOnEmptyData: false,
          },
        })
      })

      it('Should not generate rows', () => {
        expect(wrapper.vm.genRows()).toBeNull()
      })
    })
  })
})
