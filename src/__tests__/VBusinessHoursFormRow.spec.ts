import Vue from 'vue'
// Utils
import { MountOptions, shallowMount, Wrapper } from '@vue/test-utils'
// To be tested
import VBusinessHoursForm, { VBHF_EVENTS } from '..'
import VBusinessHoursFormRow from '../VBusinessHoursFormRow'
// Mocks
import businessHours from '../../test/mocks/businessHours'
import { VSwitch, VTimePicker } from 'vuetify/lib'
import VTimePickerGroup from '../VTimePickerGroup'
type Instance = InstanceType<typeof VBusinessHoursFormRow>

const vuetifyMocks = {
  $vuetify: {
    theme: {
      currentTheme: {
        error: '#ff0000',
      },
      dark: false,
    },
    lang: {
      t: (val: string) => val,
    },
    rtl: false,
  },

}

describe('VBusinessHoursFormRow', () => {
  describe('installer', () => {
    it('should register the v-business-hours-row component', () => {
      Vue.use(VBusinessHoursForm)
      expect(Vue.options.components['v-business-hours-form-row']).toBeTruthy()
    })
  })

  describe('component', () => {
    let mountFunction: (options?: MountOptions<Instance>) => Wrapper<Instance>

    beforeEach(() => {
      mountFunction = (options?: MountOptions<Instance>) => {
        return shallowMount(VBusinessHoursFormRow, {
          mocks: {
            ...vuetifyMocks,
          },
          ...options,
        })
      }
    })

    describe('LoadedRow', () => {
      const inputIndexStart = 0
      const formName = 'form'
      const dayIndex = businessHours[0].dayIndex
      let wrapper: Wrapper<Instance>

      beforeEach(() => {
        wrapper = mountFunction({
          propsData: {
            hours: businessHours,
            dayIndex,
            inputIndexStart,
            formName,
          },
        })
      })

      it('Trigger data deletion event on switch toggle', () => {
        testSwitchTriggersEvent(wrapper, false, VBHF_EVENTS.DELETE, [businessHours.map(bh => bh.id)])
      })

      it('Trigger data creation event on add hours button clicked', () => {
        const btn = wrapper.find('.v-bhf__add-hours-button')
        const event = jest.fn()
        wrapper.vm.$on(VBHF_EVENTS.CREATE, event)

        btn.vm.$emit('click')
        expect(event).toHaveBeenCalledWith(dayIndex)
      })

      it('Trigger data update event on VTimePickerGroup change event received', () => {
        const vTimePickerGroup = wrapper.find(VTimePickerGroup)
        const firstRowId = 'a'
        const newVal = "00:00"
        const expected = {"closeAt": "12:00", "dayIndex": "1", "id": "a", "openAt": newVal}

        const event = jest.fn()
        wrapper.vm.$on(VBHF_EVENTS.UPDATE, event)

        vTimePickerGroup.vm.$emit('change', newVal)
        expect(event).toHaveBeenCalledWith(firstRowId, expected)
      })
    })

    describe('EmptyRow', () => {
      const inputIndexStart = 0
      const formName = 'form'
      const dayIndex = '0'
      let wrapper: Wrapper<Instance>

      beforeEach(() => {
        wrapper = mountFunction({
          propsData: {
            hours: [],
            dayIndex,
            inputIndexStart,
            formName,
          },
        })
      })

      it('Trigger data creation event on switch toggle', () => {
        testSwitchTriggersEvent(wrapper, true, VBHF_EVENTS.CREATE, [dayIndex])
      })
    })
  })
})

const testSwitchTriggersEvent = (wrapper: Wrapper<Instance>, switchEventValue: any, eventName: string, eventArguments: any[]) => {
  const event = jest.fn()
  // @ts-ignore
  const switchButton = wrapper.find(VSwitch)

  // Bind listener
  wrapper.vm.$on(eventName, event)
  expect(event).toHaveBeenCalledTimes(0)

  // Simulate a click on the button
  switchButton.vm.$emit('change', switchEventValue)

  expect(event).toHaveBeenCalledWith(...eventArguments)
}
