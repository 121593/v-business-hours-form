import Vue from 'vue'
// Utils
import { shallowMount, MountOptions, Wrapper } from '@vue/test-utils'

// To be tested
import VBusinessHoursForm from '..'
import VTimePickerGroup from '../VTimePickerGroup'

// Mocks
const vuetifyMocks = {
  $vuetify: {
    theme: {
      currentTheme: {
        error: '#ff0000',
      },
      dark: false,
      lang: {
        t: (val: string) => val,
      },
      rtl: false,
    },
  },
}

describe('VTimePickerGroup', () => {
  describe('installer', () => {
    it('should register the v-time-picker-group component', () => {
      Vue.use(VBusinessHoursForm)
      expect(Vue.options.components['v-time-picker-group']).toBeTruthy()
    })
  })

  describe('component', () => {
    type Instance = InstanceType<typeof VTimePickerGroup>
    let mountFunction: (options?: MountOptions<Instance>) => Wrapper<Instance>

    beforeEach(() => {
      mountFunction = (options?: MountOptions<Instance>) => {
        return shallowMount(VTimePickerGroup, {
          mocks: {
            ...vuetifyMocks,
          },
          ...options,
        })
      }
    })

    describe('internal functions and events', () => {
      let wrapper: Wrapper<Instance>
      beforeEach(() => {
        wrapper = mountFunction({
          propsData: { value: '00:00' },
        })
      })

      it('genScopedTextField() should generate a VTextField element', () => {
        expect(wrapper.vm.genScopedTextField()).toMatchSnapshot()
      })

      it('genTimePicker() should generate a VTimePicker element', () => {
        expect(wrapper.vm.genScopedPicker()).toMatchSnapshot()
      })

      it('genMenu() should generate a VMenu element', () => {
        expect(wrapper.vm.genMenu()).toMatchSnapshot()
      })
    })
  })
})
