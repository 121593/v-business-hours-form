// Imported Types
/// <reference path="../node_modules/vuetify/src/globals.d.ts" />
/// <reference path="../node_modules/vuetify/types/index.d.ts" />

import Vue, { VNode } from 'vue'

import { VLayout, VMenu, VTextField, VTimePicker } from 'vuetify/lib'

export default Vue.extend({
  name: 'v-time-picker-group',
  props: { value: { type: String } },
  data: () => ({ isActive: false }),

  methods: {

    /**
     * Generates value holder & menu activator
     * Called by ScopedSlot
     */
    genScopedTextField (): VNode {
      return this.$createElement(VTextField, {
        props: { readonly: true, value: this.value, placeholder: 'xx:xx' },
        // @todo: take care of sizing via CSS ?
        attrs: { size: 5 },
        on: { click: () => this.isActive = true },
      })
    },

    /**
     * Generates timePicker
     * Called by ScopedSlot
     */
    genScopedPicker (): VNode {
      return this.$createElement(VTimePicker, {
        props: { format: '24hr', value: this.value },
        on: {
          change: (newVal: string) => {
            this.$emit('change', newVal)
            this.isActive = false
          },
        },
      })
    },

    /**
     * Generates VMenu holding timePicker and activator
     */
    genMenu (): VNode {
      return this.$createElement(VMenu, {
        props: {
          closeOnContentClick: false,
          value: this.isActive,
        },
        scopedSlots: {
          activator: this.genScopedTextField,
          default: this.$scopedSlots.picker ? this.$scopedSlots.picker : this.genScopedPicker,
        },
        on: {
          // Prevents desync between this.isActive and menu internal state
          'update:return-value': () => this.isActive = false,
        },
      })
    },
  },

  render (h): VNode {
    return h(VLayout, { props: { row: true, wrap: true } }, [this.genMenu()])
  },
})
