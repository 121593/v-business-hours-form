// Imported Types
/// <reference path="../node_modules/vuetify/src/globals.d.ts" />
/// <reference path="../node_modules/vuetify/types/index.d.ts" />
/// <reference path="../types/index.d.ts" />

import Vue, { VNode } from 'vue'

// @ts-ignore
import Localable from 'vuetify/lib/mixins/localable'
// @ts-ignore
import Themeable from 'vuetify/lib/mixins/themeable'
import { VDivider } from 'vuetify/lib'

import { VBusinessHoursFormRow, VBHF_EVENTS } from './index'
import { VBusinessHourInterface, TimePickerTarget } from '../types'

const base = Vue.extend({ mixins: [Localable, Themeable] })
interface options extends InstanceType<typeof base> {
  initialBusinessHours: VBusinessHourInterface[]
  customStyle: Object
  weekStartOn: number
  showOnEmptyData: boolean
  formName: string
}
export default base.extend<options>().extend({
// export default mixins(
//   Localable,
//   Themeable,
// ).extend({
  name: 'v-business-hours-form',
  props: {
    // Data
    initialBusinessHours: {
      type: Array as () => VBusinessHourInterface[],
      required: false,
    },

    // Options :
    customStyle: {
      type: Object,
      default: () => ({}),
    },

    weekStartOn: {
      type: Number,
      default: 0,
      validator: val => val === 0 || val === 6,
    },

    showOnEmptyData: {
      type: Boolean,
      default: true,
    },

    formName: {
      type: String,
      default: 'v-business-hours-form',
    },
  },

  data: () => ({
    businessHours: [] as VBusinessHourInterface[],
    timePickerOpen: false,
    timePickerTarget: null as null | TimePickerTarget,
  }),

  computed: {

    idMappedBusinessHours (): Map<string, VBusinessHourInterface> {
      return new Map(this.businessHours.map(v => [v.id, v]))
    },

    dayIndexMappedBusinessHours (): Map<string, VBusinessHourInterface[]> {
      const hours: Map<string, VBusinessHourInterface[]> = new Map()
      this.businessHours.forEach(bh => {
        const di = bh.dayIndex
        const businessHoursForToday: any = hours.has(di) ? (<VBusinessHourInterface[]>hours.get(di)) : []
        businessHoursForToday.push(bh)

        hours.set(di, businessHoursForToday)
      })

      return hours
    },
  },

  mounted () {
    this.businessHours = <VBusinessHourInterface[]> this.initialBusinessHours
  },

  methods: {
    /*
     Event handlers
      */
    handleValueChanged (id: string, value: string | VBusinessHourInterface, key: 'openAt'|'closeAt' | null = null) {
      if (!this.idMappedBusinessHours.has(id)) {
        throw new Error(`Hour id ${id} not found`)
      }

      let target = (<VBusinessHourInterface> this.idMappedBusinessHours.get(id))

      if (key && typeof value === 'string') {
        // Changing only something
        target[key] = value
      } else if (typeof value !== 'string') {
        target = value
      } else {
        throw new Error('No key specified and string value supplied')
      }

      this.idMappedBusinessHours.set(id, target)
      this.$emit('change', this.businessHours)
    },

    removeValues (ids: string[]) {
      this.businessHours = this.businessHours.filter(({ id }) => !ids.includes(id))
      this.$emit('change', this.businessHours)
    },

    handleAddButtonClicked (dayIndex: string) {
      console.log('handleAddButtonClicked', dayIndex)
      this.businessHours.push({
        closeAt: '',
        openAt: '',
        dayIndex,
        // Simple random id
        id: `vbh-${Math.random().toString(36).substring(7)}`,
      })
      this.$emit('change', this.businessHours)
    },

    // Components generation

    genRows (): VNode[] | null {
      if (!(this.businessHours && this.businessHours.length) && !this.showOnEmptyData) {
        console.warn('No business hours supplied and showOnEmptyData disabled')
        return null
      }

      const rows: VNode[] = []
      const WEEK_LENGTH = 7

      for (let inputIndex = 0, i = 0; i < WEEK_LENGTH; i++) {
        const hours = this.dayIndexMappedBusinessHours.get(i.toString())
        rows.push(
          this.genRow(i, hours, inputIndex),
          this.$createElement(VDivider, {})
        )
        inputIndex += hours ? hours.length : 0
      }

      // Reorder rows if week starts on Sunday
      if (this.weekStartOn === 6) {
        console.log('reorder')
        rows.unshift(<VNode>rows.pop()) // Divider
        rows.unshift(<VNode>rows.pop()) // Actual row
      }

      return rows
    },

    genRow (i: number, hours: VBusinessHourInterface[] | null = [], inputIndex: number) {
      // noinspection SpellCheckingInspection
      return this.$createElement(VBusinessHoursFormRow, {
        on: {
          [VBHF_EVENTS.UPDATE]: this.handleValueChanged,
          [VBHF_EVENTS.DELETE]: this.removeValues,
          [VBHF_EVENTS.CREATE]: this.handleAddButtonClicked,
        },
        props: {
          hours,
          dayIndex: i.toString(),
          inputIndexStart: inputIndex,
          formName: this.formName,
        },
      })
    },
  },

  render (h): VNode {
    return h('div', {}, [this.genRows()])
  },
})
