// Imported Types
/// <reference path="../node_modules/vuetify/src/globals.d.ts" />
/// <reference path="../node_modules/vuetify/types/index.d.ts" />
/// <reference path="../types/index.d.ts" />

import Vue, { VNode } from 'vue'
// @ts-ignore
import Localable from 'vuetify/lib/mixins/localable'
// @ts-ignore
import Themeable from 'vuetify/lib/mixins/themeable'

import { VBtn, VIcon, VSwitch } from 'vuetify/lib'

import VTimePickerGroup from './VTimePickerGroup'

import './VBusinessHoursForm.scss'
import { VBusinessHourInterface } from '../types'
import CssClassAware from './mixins/CssClassAware'
import { VBHF_EVENTS } from './index'

const base = Vue.extend({ mixins: [Localable, Themeable, CssClassAware] })
interface options extends InstanceType<typeof base> {
  hours: VBusinessHourInterface[]
  dayIndex: string
  inputIndexStart: number
  formName: string
  plusIcon: string

  // Cheating
  c(key: string): string
}
export default base.extend<options>().extend({
// export default mixins(
//   Localable,
//   Themeable,
//   CssClassAware
// ).extend({
  name: 'v-business-hours-form-row',
  props: {
    // Data
    hours: {
      type: Array as () => VBusinessHourInterface[],
      required: true,
    },

    dayIndex: {
      type: String,
      required: true,
    },

    // This props tracks the number of inputs previously output as HTML inputs array use indexes
    inputIndexStart: {
      type: Number,
      required: true,
    },

    formName: {
      type: String,
      required: true,
    },

    // Options

    plusIcon: {
      type: String,
      default: '$plus',
    },
  },
  methods: {

    /**
     * Generates hidden inputs holdings values, for traditional form sending
     */
    genHiddenInputs (): VNode[] {
      const inputs: VNode[] = []
      this.hours
        .forEach((h, i: number) => [['dayOfWeek', this.dayIndex], ['openAt', h.openAt], ['closeAt', h.closeAt]]
          .forEach(([key, value]) => inputs.push(this.$createElement('input', {
            attrs: {
              type: 'hidden',
              name: `[${this.formName}][${this.inputIndexStart + i}][${key}]`,
              value,
            },
          }))))

      return inputs
    },

    /**
     * @todo: Verify translation usage & naming
     * @param dayIndex
     */
    genDayContainer (dayIndex: String): VNode {
      return this.$createElement('div', { class: this.c('day') },
        this.$vuetify.lang.t(`$vuetify.vbusinesshours.day.${dayIndex}`))
    },

    genOpenCloseToggle (isOpen: Boolean): VNode {
      return this.$createElement(VSwitch, {
        on: { change: this.handleIsOpenChange },
        props: { inputValue: isOpen },
        class: this.c('switch'),
      })
    },

    genHours (hours: VBusinessHourInterface[]): VNode {
      const hourNodes = hours.map((bh, i) => this.$createElement('div', {
        class: this.c('hour-group'),
        key: bh.id,
      }, [
        this.genHourDisplay(bh.openAt, { id: bh.id, prop: 'openAt' }),
        this.$createElement('span', { class: this.c('separator') }, '-'),
        this.genHourDisplay(bh.closeAt, { id: bh.id, prop: 'closeAt' }),
        // Add button is displayed after last hours group
        i === hours.length - 1 ? this.genHoursAddButton() : null,
      ])
      )

      return this.$createElement('div', { class: this.c('hour-container') }, hourNodes)
    },

    genHourDisplay (hour: string, callbackParams: { id: string, prop: 'openAt'|'closeAt' }) {
      return this.$createElement(VTimePickerGroup, {
        props: { value: hour },
        on: { change: this.handleHourChange(callbackParams.id, callbackParams.prop) },
      })
    },

    genHoursAddButton () {
      return this.$createElement(VBtn, {
        props: {
          icon: true,
          color: 'primary',
        },
        on: {
          click: this.handleAddHoursClick,
        },
        class: this.c('add-hours-button'),
      },
      [
        this.$createElement(VIcon, this.plusIcon),
      ])
    },

    // Event handlers

    /**
     * Handle IsOpen switch change
     * Triggers value creation or deletion
     * @param v Boolean Value
     */
    handleIsOpenChange (v: Boolean) {
      if (v) { // IsOpen
        this.handleAddHoursClick()
      } else { // !IsOpen
        this.$emit(VBHF_EVENTS.DELETE, this.hours.map(h => h.id))
      }
    },

    /**
     * @fixme: this is ugly
     * Event handler generator
     * @param id String Value id
     * @param prop String Target prop name
     */
    handleHourChange (id: string, prop: 'openAt'|'closeAt') {
      // console.log('handleHourChange', id, prop)
      return (newVal: string) => {
        const newState: VBusinessHourInterface[] = this.hours
        const elem = newState.find(v => v.id === id)

        if (!elem) {
          throw new Error(`Unable to find element id ${id}`)
        }

        (<VBusinessHourInterface>elem)[prop] = newVal
        this.$emit(VBHF_EVENTS.UPDATE, id, elem)
      }
    },

    handleAddHoursClick () {
      this.$emit(VBHF_EVENTS.CREATE, this.dayIndex)
    },

  },
  render (h): VNode {
    const children = [
      this.genHiddenInputs(),
      this.genDayContainer(this.dayIndex),
      this.genOpenCloseToggle(!!this.hours.length),
      this.genHours(<VBusinessHourInterface[]> this.hours),
    ]
    return h('div', { class: this.c('row') }, children)
  },
})
