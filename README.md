# VBusinessHoursForm

A vue/[vuetify](https://vuetifyjs.com/) component displaying a business hours form.

A demo is available [@ codepen](https://codepen.io/121593/pen/OJPMVRy)

Architecture is based on Morgan Brenton's great work, detailed in [this article](https://morphatic.com/2019/09/04/building-packageable-components-to-extend-vuetify-with-typescript-part-1/)

## Basic Usage

Within a Vue template:

```vue
<template>
  <v-business-hours-form
    :initial-business-hours="businessHours"
    :show-on-empty-data="showOnEmptyData"
    :week-start-on="weekStartOn"
  />
</template>

<script>
  import { VBusinessHoursForm } from 'v-business-hours-form'
  export default {
    components: {
      VBusinessHoursForm
    },
    data: () => ({
      weekStartOn: 0,
      showOnEmptyData: true,
      businessHours: [
        { id: 'a', dayIndex: '1', openAt: '08:00', closeAt: '12:00' },
      ],
    })
  }
</script>
```

## Collaborating

Comments, advices, pull requests, and bug reports are welcome. Please [submit an issue](https://gitlab.com/121593/v-business-hours-form/issues) via the Issues tab above.
